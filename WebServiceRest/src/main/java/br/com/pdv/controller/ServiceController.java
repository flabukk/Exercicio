package br.com.pdv.controller;


import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;


import br.com.pdv.http.PDV;
import br.com.pdv.repository.PDVRepository;
import br.com.pdv.repository.entity.PDVEntity;


/**
 * Essa classe vai expor os nosso métodos para serem acessasdos via http
 * 
 * @Path - Caminho para a chamada da classe que vai representar o nosso serviço
 * */
@Path("/service")
public class ServiceController {
		
	private final  PDVRepository repository = new PDVRepository();

	/**
	 * @Consumes - determina o formato dos dados que vamos postar
	 * @Produces - determina o formato dos dados que vamos retornar
	 * 
	 * Esse método cadastra um PDV
	 * */
	@POST	
	@Consumes("application/json; charset=UTF-8")
	@Produces("application/json; charset=UTF-8")
	@Path("/CreatePDV")
	public String Cadastrar(PDV pdv){
		
		PDVEntity entity = new PDVEntity();
				
		try {
			
			
			entity.setIdCobertura(pdv.getIdCobertura());
			entity.setTradingName(pdv.getTradingName());
			entity.setOwnerName(pdv.getOwnerName());
			entity.setDocument(pdv.getDocument());
			entity.setLongitude(pdv.getLongitude());
			entity.setLatitude(pdv.getLatitude());
			
			
			
			
			repository.Salvar(entity);
			
			return "PDV cadastrado com sucesso!";
			
		} catch (Exception e) {
			
			return "Erro ao cadastrar um PDV " + e.getMessage();
		}
	
	}
	
	
	/**
	 * Esse método lista todas pessoas cadastradas na base
	 * */
	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/todosPDVs")
	public List<PDV> TodosPDVs(){
		
		List<PDV> pdvs =  new ArrayList<PDV>();
		
		List<PDVEntity> listaEntityPDVs = repository.TodosPDVs();
		System.out.println(listaEntityPDVs.size());
		for (PDVEntity entity : listaEntityPDVs) {
			
			pdvs.add(new PDV(entity.getId(), entity.getIdCobertura(), entity.getTradingName(),entity.getOwnerName(), entity.getDocument(), entity.getLongitude(), entity.getLatitude()));
		}
		
		return pdvs;
	}
	
	/**
	 * Esse método busca um PDV cadastrada pelo id
	 * */
	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/GetPDV/{id}")
	public PDV GetPDV(@PathParam("id") Integer id){
		
		PDVEntity entity = repository.GetPDV(id);
		
		if(entity != null)
			return new PDV(entity.getId(), entity.getIdCobertura(), entity.getTradingName(),entity.getOwnerName(), entity.getDocument(), entity.getLongitude(), entity.getLatitude());
		
		return null;
	}
	
	

	/**
	 * Esse método lista todas pessoas cadastradas na base
	 * */

@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/GetPDVMaisProximo/{longitude}/{latitude}")
	public List<PDVEntity> GetPDVMaisProximo(@PathParam("longitude") Double longitude, @PathParam("latitude") Double latitude){
		
		List<PDVEntity> listaEntityPDVs = repository.PDVMaisProximo(longitude, latitude);
		return listaEntityPDVs;
		
	}
	
	
}
