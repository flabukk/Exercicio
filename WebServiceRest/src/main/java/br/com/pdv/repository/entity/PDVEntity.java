package br.com.pdv.repository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tb_pdv")
public class PDVEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="idCobertura")
	private int idCobertura;
	
	@Column(name="tradingName")	
	private String  tradingName;
	
	@Column(name="ownerName")
	private String  ownerName;
	
	@Column(name="document")
	private String  document;
	
	@Column(name="longitude")
	private Double  longitude;
	
	@Column(name="latitude")
	private Double  latitude;
	
	public PDVEntity(){
		
	}
	
	public PDVEntity(int id, int idCobertura, String tradingName, String ownerName, String document, Double longitude,
			Double latitude, Double distance) {
		super();
		this.id = id;
		this.idCobertura = idCobertura;
		this.tradingName = tradingName;
		this.ownerName = ownerName;
		this.document = document;
		this.longitude = longitude;
		this.latitude = latitude;
		this.distance = distance;
	}

	@Column(name="distance")
	private Double  distance;
	
	
	

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public int getIdCobertura() {
		return idCobertura;
	}

	public void setIdCobertura(int idCobertura) {
		this.idCobertura = idCobertura;
	}
	
	public String getTradingName() {
		return tradingName;
	}

	public void setTradingName(String tradingName) {
		this.tradingName = tradingName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	
	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	
	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	
	
	
}
