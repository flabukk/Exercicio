package br.com.pdv.repository;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;

import br.com.pdv.repository.entity.PDVEntity;




public class PDVRepository {

	private final EntityManagerFactory entityManagerFactory;
	
	private final EntityManager entityManager;
	
	public PDVRepository(){
		
		/*CRIANDO O NOSSO EntityManagerFactory COM AS PORPRIEDADOS DO ARQUIVO persistence.xml */
		this.entityManagerFactory = Persistence.createEntityManagerFactory("persistence_zxventures");
		
		this.entityManager = this.entityManagerFactory.createEntityManager();
	}
	
	/**
	 * CRIA UM NOVO REGISTRO NO BANCO DE DADOS
	 * */
	public void Salvar(PDVEntity pdvEntity){
		
		this.entityManager.getTransaction().begin();
		this.entityManager.persist(pdvEntity);
		
		this.entityManager.getTransaction().commit();
	}
	
																			/**
	 * ALTERA UM REGISTRO CADASTRADO
	 * */
	public void Alterar(PDVEntity pdvEntity){
		
		this.entityManager.getTransaction().begin();
		this.entityManager.merge(pdvEntity);
		this.entityManager.getTransaction().commit();
	}
	
	/**
	 * RETORNA TODAS OS PDVS CADASTRADAS NO BANCO DE DADOS 
	 * */
	@SuppressWarnings("unchecked")
	public List<PDVEntity> TodosPDVs(){
		
		
		return this.entityManager.createQuery("SELECT p FROM PDVEntity p ORDER BY p.tradingName").getResultList();
	}
	
	
	/**
	 * RETORNA TODAS OS PDVS CADASTRADAS NO BANCO DE DADOS 
	 * */
	@SuppressWarnings("unchecked")
	public List<PDVEntity> PDVMaisProximo(Double longitude, Double latitude){
		
				
		StoredProcedureQuery pdv_proximo = entityManager.createStoredProcedureQuery("PR_BuscarPDVMaisProximo");
	
		pdv_proximo.registerStoredProcedureParameter("a", Double.class, ParameterMode.IN); 
		pdv_proximo.registerStoredProcedureParameter("b", Double.class, ParameterMode.IN);
		
		pdv_proximo.setParameter("a", longitude);
		pdv_proximo.setParameter("b", latitude);
		List<Object> retornolist = (List<Object>)pdv_proximo.getResultList();		
		Object arrList = new ArrayList<Object>(Arrays.asList(retornolist.get(0))).get(0);
		Object[] objects = (Object[]) arrList;
		PDVEntity newPDV = new  PDVEntity(
				Integer.parseInt(objects[0].toString())
				, Integer.parseInt(objects[1].toString())
				, objects[2].toString()
				, objects[3].toString()
				, objects[4].toString()
				, Double.parseDouble(objects[5].toString())
				,  Double.parseDouble(objects[6].toString())
				,  Double.parseDouble(objects[7].toString()));
		List<PDVEntity> pdv = new ArrayList<PDVEntity>();
	
		pdv.add(newPDV);
		
		
		return pdv;
		
		
	
		
		/* Query pdv_proximo = entityManager.createNativeQuery("call PR_BuscarPDVMaisProximo(:longitude,:latitude)").setParameter("longitude", longitude).setParameter("latitude", latitude); */
		/*Query pdv_proximo = entityManager.createNativeQuery("call PR_BuscarPDVMaisProximo(?1,?2)");*/
		
		/*
		Query pdv_proximo = entityManager.createNativeQuery("call teste(?1,?2)");
		pdv_proximo.setParameter(1, longitude.doubleValue());
		pdv_proximo.setParameter(2, latitude.doubleValue());	
 		
		return pdv_proximo.getResultList();
		
		

		
		
		
		/*return pdv_proximo.getResultList();*/
		
		/*return this.entityManager.createStoredProcedureQuery("PR_BuscarPDVMaisProximo").getResultList();*/
	}
	
	
	
	/**
	 * CONSULTA UM PDV CADASTRADO PELO ID
	 * */
	public PDVEntity GetPDV(Integer id){
		
		return this.entityManager.find(PDVEntity.class, id);
	}
	
	
	
}
