package br.com.pdv.http;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PDV {
	
	
	private int id;
	private int idCobertura;
	private String tradingName;
	private String ownerName;
	private String document;
	private Double longitude;
	private Double latitude;
	
		
public PDV(){
		
	}
	
	public PDV(int id, int idCobertura, String tradingName, String ownerName, String document, Double longitude, Double latitude) {
		super();
		this.id = id;
		this.idCobertura = idCobertura;
		this.tradingName = tradingName;
		this.ownerName = ownerName;
		this.document = document;
		this.longitude = longitude;
		this.latitude = latitude;
		
	}

		
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getIdCobertura() {
		return idCobertura;
	}
	public void setIdCobertura(int idCobertura) {
		this.idCobertura = idCobertura;
	}
	
	public String getTradingName() {
		return tradingName;
	}
	public void setTradingName(String tradingName) {
		this.tradingName = tradingName;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getDocument() {
		return document;
	}
	public void setDocument(String document) {
		this.document = document;
	}
	
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	/*
	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double latitude) {
		this.distance = distance;
	}
	*/
	
}
